//解法一：
// class Solution {
// public:
//     int trap(vector<int>& height) {
//         int n=height.size();
//         int ans=0;
//         for(int i=1;i<n-1;i++)//如果i初始值和结尾值为0和n的话，j就会卡
//         {
//             int left_max=0;int right_max=0;//左右最高柱子
//             for(int j=i;j<n;j++)//右
//             {
//                 right_max=max(right_max,height[j]);
//             }
//             for(int j=i;j>=0;j--)
//             {
//                 left_max=max(left_max,height[j]);
//             }
//             ans+=min(left_max,right_max)-height[i];
//         }
//         return ans;
//     }
// };



//解法二：双指针法
// class Solution {
// public:
//     int trap(vector<int>& height) {
//         int left=0,right=height.size()-1;
//         int l_max=0,r_max=0;

//         int ans=0;
//         while(left<right)
//         {
//             l_max=max(l_max,height[left]);
//             r_max=max(r_max,height[right]);

//             //分开求和,因为指针加减不方便
//             if(l_max<r_max)
//             {
//                 ans+=l_max-height[left];
//                 left++;
//             }
//             else
//             {
//                 ans+=r_max-height[right];
//                 right--;
//             }
//         }
//             return ans;
//     }
// };