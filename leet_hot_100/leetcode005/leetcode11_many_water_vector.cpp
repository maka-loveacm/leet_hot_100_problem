//双指针
// class Solution {
// public:
//     int maxArea(vector<int>& height) {
//         int left=0;int right=height.size()-1;
//         int ans=0;//必须写这个，用来更新数据
//         while(left<right)
//         {
//             int s=min(height[left],height[right])*(right-left);
//             ans=max(s,ans);
//             //哪边小，不要哪边
//             if(height[left]<height[right])
//             {
//                 left++;
//             }
//             else
//             {
//                 right--;
//             }
//         }
//         return ans;   
//     }
// };