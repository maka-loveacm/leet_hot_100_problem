//题目描述：
// 未排序的整数数组 nums ，找数字连续最长序列（不要求序列元素在原数组中连续）的长度

// 输入：nums = [100,4,200,1,3,2]
// 输出：4
// 解释：最长数字连续序列是 [1, 2, 3, 4]。它的长度为 4。

// 输入：nums = [0,3,7,2,5,8,4,6,0,1]
// 输出：9

//思路解析：
//1 . 用set去除元素 
//2. 排序
//3.在更新flag2的值时，我们需要利用利用flag1一直跑来更新flag2,最后返回的应该是flag2+1，因为我们要返回的是连续序列的长度，而flag2表示的是连续序列的最大索引与起始索引之间的距离。

#include<iostream>
#include<algorithm>
using namespace std;
#include<vector>
#include<set>

class Solution {
public:
    int longestConsecutive(vector<int>& nums) {
        int flag1 = 0; // 用于记录当前连续序列的长度
        int flag2 = 0; // 用于记录最长连续序列的长度

        set<int> numSet(nums.begin(),nums.end());//去除重复元素

        for (int num : numSet) {
            if (numSet.find(num-1)==numSet.end()) {
                // 当前元素与前一个元素不相等，表示当前连续序列的开始
                int currentNum=num;
                flag1=1; // 初始化当前连续序列长度为1，因为当前元素也属于一个连续序列

                while(numSet.find(currentNum+1)!=numSet.end())
                {
                    currentNum++;
                    flag1++; // 连续序列长度加1
                }
                flag2=max(flag2,flag1);
            }
             
            }
            // 返回最长连续序列的长度
        return flag2;
        }

};

int main() {
    vector<int> nums1 = {100, 4, 200, 1, 3, 2};
    Solution s;
    cout << s.longestConsecutive(nums1) << endl; // Output: 4

    vector<int> nums2 = {0, 3, 7, 2, 5, 8, 4, 6, 0, 1};
    cout << s.longestConsecutive(nums2) << endl; // Output: 9

    return 0;
}


