//俩数之和
// 题目描述：数组nums和目标值 target
// 请你在该数组中找出 和为目标值 target 的那两个 整数，并返回它们的数组下标。
// 数组中同一个元素在答案里不能重复出现。
// 输入：nums = [3,2,4], target = 6
// 输出：[1,2]


// 思路解析：
// 最容易想到的方法是枚举数组中的每一个数 x，寻找数组中是否存在 target - x。
// 当我们使用遍历整个数组的方式寻找 target - x 时，
// 需要注意到每一个位于 x 之前的元素都已经和 x 匹配过，
// 因此不需要再进行匹配。而每一个元素不能被使用两次，所以我们只需要在 x 后面的元素中寻找 target - x。
#include<iostream>
#include<vector>
using namespace std;
vector<int> twoSum(vector<int>& nums, int target) {
        int n=nums.size();
        for(int i=0;i<n;i++)
        {
            for(int j=i+1;j<n;j++)
            {
                if(nums[i]+nums[j]==target)
                {
                    return {i,j};
                }
            }
        }
        return {};
    }
int main() {
    //自己写例子
    vector<int> nums = {3,2,4};
    int target = 6;

    vector<int> result = twoSum(nums, target);

    if (result.size() == 2) {
        cout << "Indices of the two numbers whose sum is equal to target:" << endl;
        cout << "Index 1: " << result[0] << endl;
        cout << "Index 2: " << result[1] << endl;
    } else {
        cout << "No such pair found." << endl;
    }

    return 0;
}